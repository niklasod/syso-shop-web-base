package se.syso.shop.web;

import java.io.Serializable;
import java.util.List;

public class Event implements Serializable {

    private String type;
    private List<EventData> eventData;



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<EventData> getEventData() {
        return eventData;
    }

    public void setEventData(List<EventData> eventData) {
        this.eventData = eventData;
    }
}
