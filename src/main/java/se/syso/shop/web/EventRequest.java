package se.syso.shop.web;

import java.io.Serializable;

public class EventRequest implements Serializable {

    private String requestId;
    private String callbackSystem;

    private Event event;

    public String getCallbackSystem() {
        return callbackSystem;
    }

    public void setCallbackSystem(String callbackSystem) {
        this.callbackSystem = callbackSystem;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
